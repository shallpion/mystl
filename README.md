This is my personal implementation of a simple STL for fun. It is largely based on the work in the book **Standard Template Library** and Stepanov & Lee's original implementation.
